'use strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var m_route_index = require('./routes/index');
var m_route_stories = require('./routes/stories');
//var users = require('./routes/users');

// added by hand - start
var m_session = require('express-session');
var m_connect_mongo = require('connect-mongo');
var m_passport = require('passport');
var m_local_strategy = require('passport-local').Strategy;
var m_mongoose = require('mongoose');
// added by hand - end



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//app.use(cookieParser());

app.use(m_session({resave: false, saveUninitialized: false, secret: '!!sect0rG37@', cookie: { maxAge: 60000000000 }}) );

app.use(m_passport.initialize());
app.use(m_passport.session());


app.use(express.static(path.join(__dirname, 'public')));

app.use('/', m_route_index);
app.use('/login', m_route_index);
app.use('/logout', m_route_index);
app.use('/stories', m_route_stories);

//app.use('/users', users);

// passport config
var m_model_user = require('./models/user');
//passport.use(User.createStrategy());
m_passport.use(new m_local_strategy(m_model_user.authenticate()));
m_passport.serializeUser(m_model_user.serializeUser());
m_passport.deserializeUser(m_model_user.deserializeUser());

var m_db = 'langz002'
m_mongoose.connect('mongodb://localhost/' + m_db);
m_mongoose.connection.on('open', function() {console.log('Mongoose connected:' + m_db); });


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
