'use strict';

var m_express = require('express');
var m_router = m_express.Router();
var m_user_model = require('../models/user')



function f_logout(req, res, next) {
  req.logout();
  res.redirect('/')
}

function f_login(req, res, next) {
  m_user_model.findOne({ email: req.body.email }, 
    function(m_err, m_user) {
      console.log(m_err)
      console.log(m_user)
      if (m_err) {
        console.log('error while finding user by email')
        return res.render('deadend')
      }
      if ( ! m_user ) {
        console.log('Email: ', req.body.email)
        console.log('Pass: ', req.body.password)
        m_user_model.register(new m_user_model({ email: req.body.email }), req.body.password, function(m_err, m_user) {
            if (m_err) {
              console.log('myerror while user register!', m_err);
              return res.render('index', { alert: 'error while registering, check email address' });
            }
            if ( ! m_user ) {
              console.log('no user created');
              return res.render('index', { alert: 'user was not created' });
            }
            m_user_model.findOne({ email: req.body.email }, function(m_err, m_user) {
                req.login(m_user, function(m_err) {
                  console.log('Logging in... ')
                  if (m_err) {
                    console.log('Error: ', m_err)
                    return res.redirect('/');
                  }
                  console.log(req.session)
                  return res.redirect('/');
                });
            })
        });
      } else {
        console.log('User already exists')
        m_user.authenticate(req.body.password, function(m_err, m_user, m_reason) {
          if (m_err) {
            console.log(m_err)
            return res.render('index', { alert: 'Error while authenticating' });
          }
          if ( ! m_user ) {
            console.log(m_reason.message)
            res.render('index', { alert: m_reason.message });
          } else {
            console.log('Success: ' + m_user)
            req.login(m_user, function(m_err) {
              if (m_err) {
                console.log(m_err)
                return res.redirect('/');
              }
              res.redirect('/');
            });
          }
        })
      }
    }
  )
};


/* GET home page. */
m_router.get('/', function(req, res, next) {
  console.log('Session:', req.session)
  if ( typeof req.session.passport === 'undefined' ) {
    res.render('index', { title: 'Express' });
  } else {
    res.render('index', { title: 'Express', m_user: req.session.passport.user });
  }
});

m_router.post('/login', f_login );
m_router.get('/logout', f_logout );
m_router.post('/logout', f_logout );


module.exports = m_router;
