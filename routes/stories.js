'use strict';

var m_express = require('express');
var m_router = m_express.Router();
var m_user_model = require('../models/user')
var m_story_model = require('../models/story')

function f_add_story(m_story, m_next) {
  var m_new_story = new m_story_model({ title: m_story.title, _creator: m_story.creator_id, content: m_story.content })
  m_new_story.save(
    function(m_err) {
      if (m_err) {
        console.log('FAIL: new story')
        return m_next(err)
      }
      m_next(null)
    }
  )
}

function f_new(m_req, m_res, m_next) {
  m_res.render('stories/new', { m_user: m_req.user });
}

function f_add(m_req, m_res, m_next) {
  console.log('f_add: ', m_req.user)
  var m_story = {}
  m_story.title = m_req.body.title.trim()
  m_story.content = m_req.body.content.trim()
  m_story.creator_id = m_req.user.id
  console.log(m_story.title, m_story.creator_id)
  f_add_story(
    m_story, 
    function(m_err) {
      if ( m_err ) {
        console.log('ERROR:add:story')
        return m_res.redirect('/')
      }
      m_res.redirect('/stories/list')
    }
  )
}

function f_list(m_req, m_res) {
  m_story_model.find(
    {},
    function(m_err, m_stories) {
      if (m_err) {
        console.log('FAIL: story find: m_list_stories')
        return m_res.render('stories/list') 
      }
      m_res.render('stories/list', { m_user: m_req.user, v_stories: m_stories });
    }
  )
}

function f_view(m_req, m_res) {
  m_story_model.findOne(
    { _id: m_req.params.id }, 
    function(m_err, m_story) {
      m_res.render('stories/view', { m_user: m_req.user, v_story: m_story });
    }
  )
}

function f_view_sentences(m_req, m_res) {
  m_story_model.findOne(
    { _id: m_req.params.id }, 
    function(m_err, m_story) {
      m_res.render('stories/sentences/view', { m_user: m_req.user, v_story: m_story });
    }
  )
}

function f_edit_sentences(m_req, m_res) {
  m_story_model.findOne(
    { _id: m_req.params.id }, 
    function(m_err, m_story) {
      m_res.render('stories/sentences/edit', { m_user: m_req.user, v_story: m_story });
    }
  )
}

function f_add_sentence(m_req, m_res, m_next) {
  m_story_model.findOne(
    { _id: m_req.body.story_id }, 
    function(m_err, m_story) {
      m_story.sentences.push({ content: m_req.body.sentence, link: m_req.body.audiolink });
      m_story.save(function (err) {
        if (err) {return handleError(err) }
        m_res.redirect('/stories/' + m_story.id + '/edit/sentences');
      })
    }
  )
}

function f_remove_sentence(m_req, m_res, m_next) {
  m_story_model.findOne(
    { _id: m_req.params.story_id }, 
    function(m_err, m_story) {
        //console.log(m_story)
      m_story.sentences.id(m_req.params.sentence_id).remove();
      m_story.save(function (err) {
        if (err) {return handleError(err) }
        m_res.redirect('/stories/' + m_story.id + '/edit/sentences');
      })
    }
  )
}

m_router.get('/new', f_new );
m_router.get('/list', f_list );
m_router.get('/view/:id', f_view );
m_router.post('/add', f_add );
m_router.post('/sentence/add', f_add_sentence );
m_router.get('/:id/view/sentences', f_view_sentences);
m_router.get('/:id/edit/sentences', f_edit_sentences);
m_router.get('/:story_id/sentences/remove/:sentence_id', f_remove_sentence);
//m_router.get('/:id/view/chapters', f_view_chapters);
//m_router.get('/:id/edit/chapters', f_edit_chapters);
//m_router.get('/:id/add/chapters', f_add_chapters);

module.exports = m_router;
