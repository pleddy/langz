var m_mongoose = require('mongoose');
var m_mongoose_schema = m_mongoose.Schema;


var m_schema_sentence = new m_mongoose_schema ({ 
  content: String,
  link: String, 
});


var m_schema_story = new m_mongoose_schema ({
  _creator: { type: m_mongoose_schema.Types.ObjectId },
  title: { type: String, required: true },
  content: String,
  sentences: [m_schema_sentence],
  lang: String
});


module.exports = m_mongoose.model('Story', m_schema_story);      //first string is db table name