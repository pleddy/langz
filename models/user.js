var m_validate = require('mongoose-validate');
var m_mongoose = require('mongoose');
var m_mongoose_schema = m_mongoose.Schema;
var m_passport_local_mongoose = require('passport-local-mongoose');

var m_schema_user = new m_mongoose_schema ({
  email: { type: String, required: true, validate: [m_validate.email, 'invalid email address'] },
  words: { type: [String], index: true }
});

m_schema_user.plugin(m_passport_local_mongoose, {usernameField: 'email'});

module.exports = m_mongoose.model('User', m_schema_user);      //first string is db table name
